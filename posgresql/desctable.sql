--Uzyskanie opisu tabeli przez procedure z wykorzystaniem cursora:
CREATE OR REPLACE FUNCTION DESCTAB(nameTab Text)
  RETURNS TABLE (
  "coulmn_name" text, 
  "coulmn_type" text,
  "coulmn_default" text,
  "is_null" text,
  "number_column" int
  ) AS $$
DECLARE 
  nameoftable TEXT DEFAULT '';
  tablen   RECORD;
  counter int :=1;
  cur_t CURSOR(nameTab Text)
 FOR 
 SELECT
 DISTINCT ON (COLUMN_NAME ,attnum)
 COLUMN_NAME, 
 data_type, 
 column_default, 
 is_nullable, 
 attnum 
FROM 
information_schema.COLUMNS,
pg_attribute 
WHERE TABLE_NAME = nameTab 
AND attname=COLUMN_NAME
ORDER BY attnum;
  BEGIN
   OPEN cur_t(nameTab);
    LOOP
      FETCH cur_t INTO tablen;
      EXIT WHEN NOT FOUND;
    WHILE counter <= tablen.attnum LOOP
        "coulmn_name"  = tablen.COLUMN_NAME; 
        "coulmn_type"  = tablen.column_default;
        "coulmn_default"  = tablen.attnum;
        "is_null"  = tablen.is_nullable;
        "number_column"  = tablen.attnum;
    counter := counter + 1 ;
   RETURN Next;
     END LOOP ;    
  END LOOP;
   CLOSE cur_t;
END; $$
LANGUAGE plpgsql;

--Sprawdzenie-------
SELECT DESCTAB('reviews');




--Użyte zapytanie, lepsza czytelność--
SELECT
 DISTINCT ON (attnum)
 COLUMN_NAME, 
 data_type, 
 column_default, 
 is_nullable, 
 attnum 
FROM 
information_schema.COLUMNS,
pg_attribute 
WHERE TABLE_NAME = 'reviews' 
AND attname=COLUMN_NAME
ORDER BY attnum;
--Alternatywa Joiny--- 
SELECT  DISTINCT ON (attnum)
 COLUMN_NAME, 
 data_type, 
 column_default, 
 is_nullable , 
 attnum 
FROM information_schema.COLUMNS
INNER JOIN pg_attribute
ON attname=COLUMN_NAME
WHERE TABLE_NAME = 'reviews';



