### Procedura dodająca do tabel w POSTGRESQL i MYSQL.

###Dla POSTGRESQL.
Baza Heroku, należy mieć zainstalowanego POSTGRESQL 
#Polecenie
psql -h ec2-54-247-177-33.eu-west-1.compute.amazonaws.com -d d52vmom9ghh2fn -U xzhlcuipbrpzir

Hasło:

a55b1be746a5670704f31fc2151fb4e3fb5d60df7b33464b2703759170ca3c66

###Dla MYSQL.
Nie ma hostowanej bazy, gdyż darmowa nie pozwala na tworzeniu triggerów. 

Opis problemu, mając dwie tabele zawierające produkty i recenzję
stworzona została procedura, która:
##  Dodawanie
1. Przyjmuję dane do tabeli PRODUCTS i REVIEWS .
2. Następnie sprawdza, czy dane się już nie pojawiły. 
3. Jeżeli tak, wówczas nie dokonuje zapisu, w przeciwnym razie zapis jest dokonywany.
4. Dokonywany jest wpis także do tabeli krzyżującej PRODUCTS_REVIEWS.
##  Aktualizacja
1. Dokonuje się zmiany w tabeli reviews
2. Zmiany zostają dokonane, a stare i nowe dane zapisane do tabeli LOGCHANCHE.
3. Wykorzystywana jest do tego reguła log_change
## Usuwanie
1. Dokonuje się usuwania rekordów w tabeli reviews. Zaleca sie używania sprecyzowanego z warunkiem where.
2. Zmiany zostają dokonane, a stare dane zapisane do tabeli DELETEREVIEWS.
3. Wykorzystywana jest do tego wyzwalacz delete_rev z funkcją bef_delete 
## Czyszczenie
1. Przywraca bazę do stanu sprzed użycie procedur.
2. Tabele pozostają puste, a id wyzerowane.
3. Wykorzystywana jest do tego funkcja/procedura clean_my_database

##Ponadto
###Dla MYSQL.
sequencemysql, stworzona trywialna sekwencja podobna do POSTGRESQL
###Dla POSTGRESQL.
desctable opis tabel przy wykorzystaniu Kursorów.
Proszę najpierw uruchomić skrypt tworzący tabele, jeżeli zalogowano się na bazę Heroku, tabele są już utworzone. Następnie z pliku sprawdzenie, użyć komend dla przetestowania zmian